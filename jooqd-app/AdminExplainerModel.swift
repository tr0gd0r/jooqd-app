//
//  AdminExplainerModel.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 4/16/17.
//  Copyright © 2017 jooqd. All rights reserved.
//

import Foundation

enum Overlays {
    case welcome
    case playTab
    case voteTab
    case shareLink
    case playBtn
}

class AdminExplainerModel {
    private var shouldShowOverlay: Bool = true
    private var currentlyShowingPosition: Int? = nil
    private var overlaysToShowOrdered: [Overlays] = [.welcome, .playTab, .voteTab, .shareLink, .playBtn]
    
    init() {
        // todo save whether overlay already shown and update shouldShowOverlay
        return
    }
    
    func shouldBeginExplainerSequence() -> Bool {
        return self.shouldShowOverlay
    }
    
    func getNextOverlay() -> Overlays? {
        if self.currentlyShowingPosition == nil {
            self.currentlyShowingPosition = 0
        }
        if self.currentlyShowingPosition! >= self.overlaysToShowOrdered.count {
            return nil
        }
        let overlayToShow: Overlays = self.overlaysToShowOrdered[self.currentlyShowingPosition!]
        self.currentlyShowingPosition! += 1
        return overlayToShow
    }
}
