//
//  PlaySongViewController.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 1/29/17.
//  Copyright © 2017 jooqd. All rights reserved.
//

import MediaPlayer
import UIKit

class PlaySongViewController: UIViewController {
    var spotifyModel: SpotifyPlayer? = nil
    var appleMusicModel: AppleMusicPlayer? = nil
    let ExplainerOverlayModel: AdminExplainerModel = AdminExplainerModel()
    var showingExplainer: Bool = false
    let nc = NotificationCenter.default
    
    @IBOutlet weak var albumCoverImg: UIImageView!
    @IBOutlet weak var song: UILabel!
    @IBOutlet weak var artist: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var playAndPauseImg: UIImageView!
    @IBOutlet weak var skipSongImg: UIImageView!
    @IBOutlet weak var playlistName: UILabel!
    @IBOutlet weak var webAppLink: UILabel!
    @IBOutlet weak var explainerWelcome: UIImageView!
    @IBOutlet weak var explainerPlayTab: UIImageView!
    @IBOutlet weak var explainerVoteTab: UIImageView!
    @IBOutlet weak var explainerPlayBtn: UIImageView!
    @IBOutlet weak var explainerShareLink: UIImageView!
   
    var overlayLookup: [Overlays: UIImageView] = [:]
    
    func setCoverArt(coverUri: String) {
        let url = URL(string: coverUri)
        let session = URLSession.shared
        
        let task = session.dataTask(with: url!, completionHandler: {data, response, error -> Void in
            let httpResponse:HTTPURLResponse? = response as? HTTPURLResponse
            if httpResponse?.statusCode == 400 {
                return
            }
            DispatchQueue.main.async {
                self.albumCoverImg.image = UIImage(data: data!)
            }
        })
        task.resume()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let vendor = Settings.vendor {
            switch vendor {
            case Vendors.apple:
                self.appleMusicModel = AppleMusicPlayer()
                self.appleMusicModel?.delegate = self
                let stateSelector = NSNotification.Name.MPMusicPlayerControllerPlaybackStateDidChange
                nc.addObserver(self, selector: #selector(self.appleMusicPlaybackStateDidChange), name: stateSelector, object: nil)
            case Vendors.spotify:
                self.spotifyModel = SpotifyPlayer()
                self.spotifyModel?.delegate = self
                self.spotifyModel?.player.delegate = self
                self.spotifyModel?.player.playbackDelegate = self
            }
        }
        
        self.view.backgroundColor = Settings.backgroundColor
        
        song.textColor = Settings.fontColor
        artist.textColor = Settings.fontColor
        time.textColor = Settings.fontColor
        playlistName.textColor = Settings.fontColor
        playlistName.adjustsFontSizeToFitWidth = true
        webAppLink.textColor = Settings.fontColor
        webAppLink.alpha = 0
        skipSongImg.alpha = 0
        
        let shouldShowExplainer: Bool = ExplainerOverlayModel.shouldBeginExplainerSequence()
        if shouldShowExplainer {
            showingExplainer = true
            self.overlayLookup[Overlays.welcome] = explainerWelcome
            self.overlayLookup[Overlays.playTab] = explainerPlayTab
            self.overlayLookup[Overlays.voteTab] = explainerVoteTab
            self.overlayLookup[Overlays.shareLink] = explainerShareLink
            self.overlayLookup[Overlays.playBtn] = explainerPlayBtn
            for (_, v) in self.overlayLookup {
                v.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.showExplainerOverlay))
                v.addGestureRecognizer(tap)
            }
            self.showExplainerOverlay()
        }
        
        let pressPlay = UITapGestureRecognizer(target: self, action: #selector(self.playPauseButtonPressed))
        let pressSkip = UITapGestureRecognizer(target: self, action: #selector(self.skipSong))
        self.playAndPauseImg.addGestureRecognizer(pressPlay)
        self.skipSongImg.addGestureRecognizer(pressSkip)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.playlistName.text = Settings.playlistName
        self.webAppLink.text = "app.jooqd.com/\(Settings.playlistId)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func playPauseButtonPressed() {
        if self.showingExplainer == true { return }
        if let model = self.spotifyModel {
            model.togglePlayState()
        }
            
        if let model = self.appleMusicModel {
            model.togglePlayState()
        }
    }
    
    func skipSong() {
        if let model = self.spotifyModel {
            model.skipSong()
        }
            
        if let model = self.appleMusicModel {
            model.skipSong()
        }
    }
}
extension PlaySongViewController {
    func showExplainerOverlay() {
        let overlayToShow: Overlays? = ExplainerOverlayModel.getNextOverlay()
        if let o = overlayToShow {
            for (k, v) in self.overlayLookup {
                if k == o {
                    v.isHidden = false
                } else {
                    v.isHidden = true
                }
                if k == .shareLink && o == .shareLink {
                    self.webAppLink.alpha = 1
                }
            }
        } else {
            // hide everything
            for (_, v) in self.overlayLookup {
                v.isHidden = true
            }
            self.skipSongImg.alpha = 1
            self.showingExplainer = false
        }
    }
   
}

/* JooqdPlayerDelegate stuff */
extension PlaySongViewController: JooqdPlayerDelegate {
    func progressChange(current: Double, total: Double) {
        DispatchQueue.main.async {
            self.time.text = String(format: "%0.2f / %0.2f", current, total)
        }
    }
    
    func newSongReceived(song: String, artist: String, albumArtUrl: String) {
        DispatchQueue.main.async {
            self.artist.text = artist
            self.song.text = song
        }
        self.setCoverArt(coverUri: albumArtUrl)
        if self.appleMusicModel !== nil {
            self.startTimer()
        }
    }
    
    func songFinishedPlaying() {
        return
    }
}

/* spotify stuff */
extension PlaySongViewController: SPTAudioStreamingDelegate, SPTAudioStreamingPlaybackDelegate {
    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didChangePosition position: TimeInterval) {
        if let model = self.spotifyModel {
            if model.songIsSet && model.songIsPlaying {
                let duration = audioStreaming.metadata.currentTrack!.duration / 60
                let currentPosition = position / 60
                self.progressChange(current: currentPosition, total: duration)
            }
        }
    }
    
    func audioStreamingDidLogin(_ audioStreaming: SPTAudioStreamingController!) {
        //playAndSkipImg.isEnabled = true
        print("logged in!")
    }
    
    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didReceiveError error: Error!) {
        print("didReceiveError: \(error)")
    }
    
    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didChangePlaybackStatus isPlaying: Bool) {
        if let model = self.spotifyModel {
            model.songIsPlaying = isPlaying
            switch model.songIsPlaying {
            case false:
                DispatchQueue.main.async {
                    self.playAndPauseImg.image = #imageLiteral(resourceName: "play_button_yellow")
                }
            case true:
                DispatchQueue.main.async {
                    self.playAndPauseImg.image = #imageLiteral(resourceName: "pause_button_yellow")
                }
            }
        }
    }
    
    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didStopPlayingTrack trackUri: String!) {
        if let model = self.spotifyModel {
            model.songIsPlaying = false
            model.playNewSong()
        }
    }
}

/* apple music stuff */
extension PlaySongViewController {
    func appleMusicPlaybackStateDidChange(msg: Notification) {
        if self.showingExplainer == true { return }
        guard let model = self.appleMusicModel else { return }
        let playbackState = model.player.playbackState
        switch playbackState {
        case .stopped:
            if let t = model.progressTimer {
                t.invalidate()
            }
            model.progressTimer = nil
            model.songIsPlaying = false
            DispatchQueue.main.async {
                self.playAndPauseImg.image = #imageLiteral(resourceName: "play_button_yellow")
            }
            model.playNewSong()
        case .playing:
            DispatchQueue.main.async {
                self.playAndPauseImg.image = #imageLiteral(resourceName: "pause_button_yellow")
            }
            model.songIsPlaying = true
        case .paused:
            model.songIsPlaying = false
            DispatchQueue.main.async {
                self.playAndPauseImg.image = #imageLiteral(resourceName: "play_button_yellow")
            }
        case .interrupted:
            model.songIsPlaying = false
            model.pauseCurrentSong()
            DispatchQueue.main.async {
                self.playAndPauseImg.image = #imageLiteral(resourceName: "play_button_yellow")
            }
        case .seekingForward:
            return
        case .seekingBackward:
            return
        }
    }
    
    func appleMusicCheckPosition() {
        if let player = self.appleMusicModel?.player {
            guard let duration = player.nowPlayingItem?.playbackDuration else { return }
            let currentPosition = player.currentPlaybackTime
            self.progressChange(current: currentPosition/60, total: duration/60)
        }
    }
    
    func startTimer() {
        guard let model = self.appleMusicModel else { return }
        if model.progressTimer !== nil {
            model.progressTimer?.invalidate()
            model.progressTimer = nil
        }
        DispatchQueue.main.async {
            // idk right now but timer doesn't get added to run loop unless called with DispatchQueue.main.async
            model.progressTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in self.appleMusicCheckPosition() }
        }
    }
}

