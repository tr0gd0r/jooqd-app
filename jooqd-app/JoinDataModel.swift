//
//  JoinDataModel.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 12/1/16.
//  Copyright © 2016 jooqd. All rights reserved.
//

import CoreLocation
import Foundation

struct PlaylistResult {
    let id: String
    let name: String
    let vendor: Vendors
    let regionCode: String
}

struct PlaylistLocation {
    let longtitude: CLLocationDegrees
    let latitude: CLLocationDegrees
}

extension PlaylistLocation {
    var json: Data? {
        let data: [String: String] = [
            "latitude": "\(self.latitude)",
            "longtitude": "\(self.longtitude)",
        ]
        do {
            return try JSONSerialization.data(withJSONObject: data, options: [])
        } catch _ {
            return nil
        }
    }
}

class PlaylistSearchModel {
    weak var delegate: PlaylistSearchDelegate?
    var playlists: [PlaylistResult] = []
    var playlistSearchAlreadyMade: Bool = false

    func parseResults(res: [[String: Any]]) {
        for playlist in res {
            let _vendor: String = playlist["vendor"] as! String
            guard let vendor: Vendors = Vendors(rawValue: _vendor) else { continue }
            let regionCode: String = playlist["region_code"] as! String
            let playlistId: String = playlist["playlist_id"] as! String
            let playlistName: String = playlist["playlist_name"] as! String
            self.playlists.append(PlaylistResult(id: playlistId, name: playlistName, vendor: vendor, regionCode: regionCode))
        }
        if let delegate = self.delegate {
            delegate.reloadPlaylistSearchResults()
        }
    }
    
    func getPlaylists(userLatLong: PlaylistLocation) {
        if self.playlistSearchAlreadyMade == true {
            return
        }
        self.playlistSearchAlreadyMade = true
        if let getData = userLatLong.json {
            let urlPath = "\(Settings.apiBase)/playlists/search"
            let url = URL(string: urlPath)
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.httpBody = getData
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            let task = URLSession.shared.dataTask(with: request, completionHandler: {data, response, error -> Void in
                guard let data = data else {
                    print("Data is empty")
                    return
                }
                do {
                    let res = try JSONSerialization.jsonObject(with: data, options: []) as! [[String: Any]]
                    self.parseResults(res: res)
                } catch _ {}
            })
            task.resume()
        }
    }
}

protocol PlaylistSearchDelegate: class {
    func reloadPlaylistSearchResults()
}
