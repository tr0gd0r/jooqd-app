//
//  JoinTableViewController.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 12/1/16.
//  Copyright © 2016 jooqd. All rights reserved.
//

import CoreLocation
import UIKit

class JoinTableViewController: UITableViewController, PlaylistSearchDelegate, CLLocationManagerDelegate {
    let model = PlaylistSearchModel()
    let locationManager = CLLocationManager()
    var alreadyPresentedLocationSpiel: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.model.delegate = self
        
        self.tableView.backgroundColor = Settings.backgroundColor
        
        locationManager.delegate = self
        locationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.startGeolocationStuff()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.playlists.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaylistCell", for: indexPath)
        let playlist = self.model.playlists[indexPath.row] as PlaylistResult
        cell.textLabel?.text = playlist.name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let playlistId: String = self.model.playlists[indexPath[1]].id
        let playlistName: String = self.model.playlists[indexPath[1]].name
        let vendor: Vendors = self.model.playlists[indexPath[1]].vendor
        let regionCode: String = self.model.playlists[indexPath[1]].regionCode
        let nc = NotificationCenter.default
        nc.post(name: NSNotification.Name(rawValue: "PlaylistSelected"), object: nil, userInfo: ["playlistId": playlistId, "playlistName": playlistName, "vendor": vendor, "regionCode": regionCode])
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Settings.backgroundColor
        cell.textLabel?.textColor = Settings.fontColor
    }

    func reloadPlaylistSearchResults() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func startGeolocationStuff() {
        let status = CLLocationManager.authorizationStatus()
        switch status {
            case .notDetermined:
                self.showLocationInfo()
            case .denied:
                /* some message about how we need location to filter search */
                return
            case .authorizedWhenInUse:
                locationManager.startUpdatingLocation()
            default:
                /* idk */
                return
        }
    }
    
    func showLocationInfo() {
        if self.alreadyPresentedLocationSpiel {
            return
        }
        self.alreadyPresentedLocationSpiel = true
        DispatchQueue.main.async {
            let title = "Where you at?"
            let message = "We'll request your location to show you playlists around you"
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Got it", style: UIAlertActionStyle.default, handler: { (alert) in
                self.locationManager.requestWhenInUseAuthorization()
            }))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.startGeolocationStuff()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // just take the last location as the users location
        let count = locations.count
        if count > 0 {
            manager.stopUpdatingLocation()
            let userLocation: CLLocation = locations.last!
            let lat = userLocation.coordinate.latitude
            let long = userLocation.coordinate.longitude
            let loc = PlaylistLocation(longtitude: long, latitude: lat)
            self.model.getPlaylists(userLatLong: loc)
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        /* handle geo errors */
    }
}
