//
//  ViewController.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 1/7/17.
//  Copyright © 2017 jooqd. All rights reserved.
//

import StoreKit
import MediaPlayer
import CoreLocation
import SafariServices
import UIKit

class CreatePlaylistViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {
    let locationManager = CLLocationManager()
    let model = PlaylistCreateModel()
    var alreadyPresentedLocationSpiel: Bool = false
    var selectedVendor: Vendors? = nil // vendor user taps on via segmented control
    
    @IBOutlet weak var spotifyConnectButton: UIView!
    @IBOutlet weak var playlistName: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var appleMusicConnectButton: UIView!
    @IBOutlet weak var vendorSelector: UISegmentedControl!
    @IBOutlet weak var vendorSelectorLabel: UILabel!
    @IBOutlet weak var signInSpinner: UIActivityIndicatorView!
    @IBOutlet weak var vendorContainer: UIStackView!
    
    // tap on segmented control - vendorSelector
    @IBAction func vendorSelected(_ sender: Any) {
        let i: Int = vendorSelector.selectedSegmentIndex
        
        switch i {
        case 0:
            self.selectedVendor = Vendors.apple
        case 1:
            self.selectedVendor = Vendors.spotify
        default:
            break
        }
        
        if self.selectedVendor != nil {
            self.vendorWasSelected()
        }
    }
    
    // tap on create playlist - submitBtn
    @IBAction func submitBtnClicked(_ sender: Any) {
        locationManager.stopUpdatingLocation()
        let name: String = playlistName.text!
        self.model.submit(playlistName: name)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.playlistName.delegate = self
        
        self.view.backgroundColor = Settings.backgroundColor
        
        submitBtn.backgroundColor = Settings.buttonBackgroundColor
        submitBtn.setTitleColor(Settings.fontColor, for: .normal)
        submitBtn.layer.cornerRadius = 5
        
        playlistName.backgroundColor = Settings.fontColor
        playlistName.textColor = Settings.buttonBackgroundColor
        playlistName.borderStyle = UITextBorderStyle.none
        playlistName.layer.cornerRadius = 5
        
        appleMusicConnectButton.backgroundColor = Settings.appleMusicRed
        appleMusicConnectButton.layer.cornerRadius = 30
        
        spotifyConnectButton.backgroundColor = Settings.spotifyGreen
        spotifyConnectButton.layer.cornerRadius = 30
        
        vendorSelector.tintColor = Settings.fontColor
        
        vendorSelectorLabel.textColor = Settings.fontColor
        
        locationManager.delegate = self
        locationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let spotifyTap = UITapGestureRecognizer(target: self, action: #selector(self.loginToSpotify))
        spotifyConnectButton.addGestureRecognizer(spotifyTap)
        
        let appleMusicTap = UITapGestureRecognizer(target: self, action: #selector(self.initAppleMusic))
        appleMusicConnectButton.addGestureRecognizer(appleMusicTap)
        
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(self.spotifyAuthComplete), name: NSNotification.Name(rawValue: "SpotifyLoginCompleted"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.startGeolocationStuff()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        DispatchQueue.main.async {
            self.submitBtn.isHidden = false
            self.submitBtn.isEnabled = true
        }
        return false
    }
    
    func vendorWasSelected() {
        if let vendor = self.selectedVendor {
            switch vendor {
            case .apple:
                self.appleMusicConnectButton.isHidden = false
                self.spotifyConnectButton.isHidden = true
            case .spotify:
                self.spotifyConnectButton.isHidden = false
                self.appleMusicConnectButton.isHidden = true
            }
        }
    }
    
    func vendorSignInComplete(vendor: Vendors) {
        self.model.vendorAuthorized = true
        self.model.vendorLoggedInWith = vendor
        DispatchQueue.main.async {
            self.signInSpinner.stopAnimating()
            self.playlistName.isHidden = false
            self.playlistName.isEnabled = true
            self.playlistName.becomeFirstResponder()
        }
    }
}

extension CreatePlaylistViewController {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.startGeolocationStuff()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // just take the last location as the users location
        let count = locations.count
        if count > 0 {
            self.model.userLocation = locations.last
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        /* handle geo errors */
    }
    
    func startGeolocationStuff() {
        let status = CLLocationManager.authorizationStatus()
        switch status {
            case .notDetermined:
                self.showLocationInfo()
            case .denied:
                self.showGeoDeniedMessage()
            case .authorizedWhenInUse:
                locationManager.startUpdatingLocation()
            default:
                /* idk */
                return
        }
    }
    
    func showLocationInfo() {
        if self.alreadyPresentedLocationSpiel {
            return
        }
        self.alreadyPresentedLocationSpiel = true
        DispatchQueue.main.async {
            let title = "Where you at?"
            let message = "We'll request your location so others can easily search for playlists around them"
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Got it", style: UIAlertActionStyle.default, handler: { (alert) in
                self.locationManager.requestWhenInUseAuthorization()
            }))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showGeoDeniedMessage() {
        DispatchQueue.main.async {
            let title = "Jooqd was denied  access to your location"
            let message = "Please enable location services for Jooqd. We use it so people can find playlists in their proximity."
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Got it", style: UIAlertActionStyle.default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

extension CreatePlaylistViewController {
    func loginToSpotify() {
        DispatchQueue.main.async {
            self.signInSpinner.startAnimating()
        }
        let redirectURL = "jooqd-login://returnafterlogin"
        Settings.spotifyAuth.clientID = Settings.clientId
        Settings.spotifyAuth.redirectURL = URL(string: redirectURL)
        Settings.spotifyAuth.sessionUserDefaultsKey = "JooqdSpotifySession"
        Settings.spotifyAuth.requestedScopes = [SPTAuthStreamingScope, SPTAuthUserReadPrivateScope]
        let tokenSwapURL: URL = URL(string: "\(Settings.apiBase)/sptauth/swap")!
        Settings.spotifyAuth.tokenSwapURL = tokenSwapURL
        let loginURL = Settings.spotifyAuth.spotifyWebAuthenticationURL()
        let vc = SFSafariViewController(url: loginURL!, entersReaderIfAvailable: false)
        present(vc, animated: true, completion: nil)
    }
    
    func spotifyAuthComplete(_ msg: Notification) {
        SPTUser.requestCurrentUser(withAccessToken: Settings.spotifyAuth.session.accessToken) { err, res in
            if err != nil {
                // error getting user info
                return
            }
            guard let user: SPTUser = res as? SPTUser else { return }
            self.model.region = user.territory
            self.vendorSignInComplete(vendor: .spotify)
        }
    }
}

extension CreatePlaylistViewController {
    func initAppleMusic() {
        DispatchQueue.main.async {
            self.signInSpinner.startAnimating()
        }
        /*
         * check if we're authorized to use apple music
         * authorizationStatus can return notDetermined, denied, restricted or authorized
         */
        let authStatus = SKCloudServiceController.authorizationStatus()
        switch authStatus {
        case .restricted:
            /* should not prompt according to docs */
            print("SKCloud​Service​Authorization​Status: restricted")
            DispatchQueue.main.async {
                self.signInSpinner.stopAnimating()
            }
            return
        case .denied:
            print("SKCloud​Service​Authorization​Status: denied")
            self.appleMusicAccessDenied()
            DispatchQueue.main.async {
                self.signInSpinner.stopAnimating()
            }
            return
        case .notDetermined:
            print("SKCloud​Service​Authorization​Status: notDetermined")
            self.appleMusicRequestPermission()
            return
        case .authorized:
            print("authorized, keep on going...")
        }
        
        // we're authorized, now check if we can playback
        self.appleMusicCheckIfDeviceCanPlayback()
        
    }
    
    /*
     * check if the device is capable of playback
     * in order to check, you need permissions from user otherwise you get cloud​Service​Permission​Denied
     */
    func checkCapabilities(capability: SKCloudServiceCapability, err: Error?) {
        if let err = err {
            /* handle error:
             *  Possible values are unknown, cloud​Service​Permission​Denied, and cloud​Service​Network​Connection​Failed.
             */
            print("error when calling requestCapabilities: \(err)")
            return
        }
        switch capability {
        case SKCloudServiceCapability.musicCatalogSubscriptionEligible:
            print("no apple music subscription")
            // has permissions + capability but no subscription
            let title = "Apple Music Subscription"
            let message = "Looks like you're trying to sign in with Apple Music but you don't have a subscription. You'll need to sign up: apple.com/apple-music"
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Got it", style: UIAlertActionStyle.default, handler: {alert in
                DispatchQueue.main.async {
                    self.signInSpinner.stopAnimating()
                }
            }))
            DispatchQueue.main.async {
                self.present(alertController, animated: true, completion: nil)
            }
        case SKCloudServiceCapability.musicCatalogPlayback:
            self.appleMusicFetchStorefrontRegion()
        case SKCloudServiceCapability.addToCloudMusicLibrary:
            self.appleMusicFetchStorefrontRegion()
        default:
            print("unknown SKCloudServiceCapability")
            return
        }
    }
    
    func appleMusicCheckIfDeviceCanPlayback() {
        let serviceController = SKCloudServiceController()
        serviceController.requestCapabilities(completionHandler: checkCapabilities)
    }
    
    func appleMusicRequestPermission() {
        /* ask for authorization and then go back to beginning */
        SKCloudServiceController.requestAuthorization { _ in self.initAppleMusic() }
    }
    
    func appleMusicAccessDenied() {
        /* https://www.natashatherobot.com/ios-taking-the-user-to-settings/ */
        let title = "Gotta get that boom boom pow"
        let message = "We need Apple Music permissions for Jooqd to work. Please enable it in Settings to continue. k?"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
            if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(appSettings, options: [:]) { success in
                    // success is just that we got to Settings page
                    print("\(success)")
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(settingsAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func appleMusicFetchStorefrontRegion() {
        let serviceController = SKCloudServiceController()
        serviceController.requestStorefrontIdentifier { (storefrontId, err) in
            if let _ = err {
                print("error getting storefrontId")
                return
            }
            
            if let storefrontId = storefrontId {
                if storefrontId.characters.count < 6 {
                    print("malformed storefrontId")
                    return
                }
                let start = storefrontId.startIndex
                let offset = 6
                let index = storefrontId.index(start, offsetBy: offset)
                let substring = storefrontId.substring(to: index)
                print("storefrontId: \(substring)")
                self.model.region = substring
                self.appleMusicLoginComplete()
            } else {
                print("storefrontId is nil value")
            }
        }
        
    }
    
    func appleMusicLoginComplete() {
        print("user can play songs through apple music!")
        DispatchQueue.main.async {
            self.signInSpinner.stopAnimating()
        }
        self.vendorSignInComplete(vendor: .apple)
    }
}

