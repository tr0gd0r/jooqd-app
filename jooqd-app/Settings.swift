//
//  Settings.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 12/19/16.
//  Copyright © 2016 jooqd. All rights reserved.
//

import Foundation

struct Settings {
    static let apiBase:String = "https://api.jooqd.com"
    static let clientId:String = "8cea2e9e922a4abdbbbe38df505f8f84"
    static var playlistName:String = ""
    static var playlistId:String = ""
    static var isAdmin:Bool = false
    static var isAdminForPlaylistId: String? = nil
    static var vendorRegion: String? = nil
    static var vendor: Vendors? = nil
    static var spotifyAuth: SPTAuth = SPTAuth.defaultInstance()

    // yellow color front logo: UIColor(red:1.00, green:0.93, blue:0.39, alpha:1.0)
    static let backgroundColor = UIColor(red:0.11, green:0.11, blue:0.11, alpha:1.0)
    static let iconColor = UIColor(red:0.88, green:0.98, blue:0.99, alpha:1.0) 
    static let tabBarBackgroundColor = UIColor(red:0.15, green:0.20, blue:0.22, alpha:1.0)
    static let buttonBackgroundColor = UIColor(red:0.15, green:0.20, blue:0.22, alpha:1.0)
    static let fontColor = UIColor(red:0.88, green:0.98, blue:0.99, alpha:1.0)
    static let spotifyGreen = UIColor(red:0.11, green:0.73, blue:0.33, alpha:1.0)
    static let appleMusicRed = UIColor(red:1.00, green:0.17, blue:0.33, alpha:1.0) // #ff2c55
}

