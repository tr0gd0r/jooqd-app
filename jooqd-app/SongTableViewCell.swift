//
//  SongTableViewCell.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 11/25/16.
//  Copyright © 2016 jooqd. All rights reserved.
//

import UIKit

class SongTableViewCell: UITableViewCell {
    var songId: String? = nil
    var userDidVoteOnSong: Bool? = nil
    var nc = NotificationCenter.default
    
    @IBOutlet weak var votesLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var upArrow: UIImageView!
    @IBOutlet weak var downArrow: UIImageView!
    
    func upvote() {
        if self.userDidVoteOnSong == true {
            return
        }
        if let songId: String = self.songId {
            nc.post(name: NSNotification.Name(rawValue: "SongWasVoted"),
                    object: nil,
                    userInfo: ["songId": songId, "direction": "up"])
        }
    }
    
    func downvote() {
        if self.userDidVoteOnSong == true {
            return
        }
        if let songId: String = self.songId {
            nc.post(name: NSNotification.Name(rawValue: "SongWasVoted"),
                    object: nil,
                    userInfo: ["songId": songId, "direction": "down"])
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        downArrow.transform = (imageView?.transform.rotated(by: CGFloat(Double.pi)))!
        let pressUp = UITapGestureRecognizer(target: self, action: #selector(self.upvote))
        let pressDown = UITapGestureRecognizer(target: self, action: #selector(self.downvote))
        self.upArrow.addGestureRecognizer(pressUp)
        self.downArrow.addGestureRecognizer(pressDown)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
