//
//  AddSongTableViewController.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 1/12/17.
//  Copyright © 2017 jooqd. All rights reserved.
//

import UIKit

class AddSongTableViewController: UITableViewController, UISearchBarDelegate, SongSearchDelegate {
    var spotifyModel: SpotifyMusicSearcher? = nil
    var appleMusicModel: AppleMusicSearcher? = nil
    var lastSearchTerm: String? = nil
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    func searchForSongs(searchTerm: String) {
        if let model = self.appleMusicModel {
            let url: URL? = try! model.getSearchUrl(searchTerm: searchTerm)
            if let url = url {
                model.searchForSongs(searchUrl: url)
            }
        }
        
        if let model = self.spotifyModel {
            let url: URL? = try! model.getSearchUrl(searchTerm: searchTerm)
            if let url = url {
                model.searchForSongs(searchUrl: url)
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.characters.count > 3 {
            if let lastSearchTerm_ = lastSearchTerm {
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(searchForSongs), object: lastSearchTerm_)
            }
            perform(#selector(searchForSongs), with: searchText, afterDelay: 1)
            self.lastSearchTerm = searchText
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let vendor = Settings.vendor {
            switch vendor {
            case Vendors.apple:
                self.appleMusicModel = AppleMusicSearcher()
                self.appleMusicModel?.delegate = self
            case Vendors.spotify:
                self.spotifyModel = SpotifyMusicSearcher()
                self.spotifyModel?.delegate = self
            }
        }
        
        searchBar.delegate = self
        
        self.tableView.backgroundColor = Settings.backgroundColor
        searchBar.barTintColor = Settings.tabBarBackgroundColor
        
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = Settings.fontColor
        
        // removes separators between cells
        tableView.separatorStyle = .none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let model = self.appleMusicModel {
            return model.searchResults.count
        }
        
        if let model = self.spotifyModel {
            return model.searchResults.count
        }
        
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SongSearchCell", for: indexPath) as! AddSongTableViewCell
        var song: SongResult
        
        if let model = self.appleMusicModel {
            song = model.searchResults[indexPath.row]
            cell.SongName.text = song.name
            cell.SongName.textColor = Settings.fontColor
            
            cell.SongArtist.text = song.artist
            cell.SongArtist.textColor = Settings.fontColor
        }
        
        if let model = self.spotifyModel {
            song = model.searchResults[indexPath.row]
            cell.SongName.text = song.name
            cell.SongName.textColor = Settings.fontColor
            
            cell.SongArtist.text = song.artist
            cell.SongArtist.textColor = Settings.fontColor
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model = self.appleMusicModel {
            let selectedSong = model.searchResults[indexPath.row]
            model.submitNewSong(song: selectedSong)
        }
        
        if let model = self.spotifyModel {
            let selectedSong = model.searchResults[indexPath.row]
            model.submitNewSong(song: selectedSong)
        }
        
        return
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Settings.backgroundColor
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    func updateResults() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func songAlreadyInPlaylist() {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "Song Already Added", message: "", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func songWasAccepeted() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "returnToVoteForSong", sender: self)
        }
    }
}
