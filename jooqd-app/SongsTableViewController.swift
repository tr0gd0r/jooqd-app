//
//  SongsTableViewController.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 11/24/16.
//  Copyright © 2016 jooqd. All rights reserved.
//

import UIKit

class SongsTableViewController: UITableViewController, SongsInPlaylistHandler {
    var model: PlaylistModel?
    var addSongOverlayActive: Bool = true
    var addSongOverlayVC: UIViewController? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        self.model = PlaylistModel(playlistId: Settings.playlistId)
        if let model = self.model {
            model.delegate = self
        }
        self.tableView.backgroundColor = Settings.backgroundColor
        // removes separators between cells
        tableView.separatorStyle = .none
        
        // initialize the add song overlay view
        if let storyboard = storyboard {
            self.addSongOverlayVC = storyboard.instantiateViewController(withIdentifier: "AddSongOverlay")
        }
        if let vc = self.addSongOverlayVC {
            self.view.addSubview(vc.view)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        self.navigationItem.title = Settings.playlistName
        /*
        * whether the model should update because
        * the user changed the playlist
        */
        var shouldChange: Bool = false
        if let model = self.model {
            if Settings.playlistId != model.playlistId {
                model.shutDown()
                shouldChange = true
            }
        }
        if shouldChange {
            self.model = nil
            self.model = PlaylistModel(playlistId: Settings.playlistId)
            self.model?.delegate = self
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int
        if let model = self.model {
            count = model.songsByVote.count
        } else {
            count = 0
        }
        
        if count == 0 {
            self.showAddSongOverlay()
        } else {
            if self.addSongOverlayActive == true {
                self.hideAddSongOverlay()
            }
        }
        return count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SongCell", for: indexPath) as! SongTableViewCell
        
        if let model = self.model {
            let song = model.songsByVote[indexPath.row]
            
            cell.songId = song.id
            
            cell.nameLabel.text = song.name
            cell.nameLabel.textColor = Settings.fontColor
            
            cell.artistLabel.text = song.artist
            cell.artistLabel.textColor = Settings.fontColor
            
            cell.votesLabel.text = "\(song.votes)"
            cell.votesLabel.textColor = Settings.fontColor
            
            cell.votesLabel.text = "\(song.votes)"
            cell.votesLabel.textColor = Settings.fontColor
            
            if song.userDidVoteOnSong {
                cell.userDidVoteOnSong = true
                if song.userDidUpvote {
                    cell.upArrow.image = #imageLiteral(resourceName: "arrow")
                    cell.downArrow.image = #imageLiteral(resourceName: "arrow_empty")
                } else {
                    cell.upArrow.image = #imageLiteral(resourceName: "arrow_empty")
                    cell.downArrow.image = #imageLiteral(resourceName: "arrow")
                }
            } else {
                cell.userDidVoteOnSong = false
                cell.upArrow.image = #imageLiteral(resourceName: "arrow_empty")
                cell.downArrow.image = #imageLiteral(resourceName: "arrow_empty")
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Settings.backgroundColor
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    func songsHaveChanged() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    @IBAction func returnToVoteForSong(segue: UIStoryboardSegue) {}

}

private extension SongsTableViewController {
    
    func showAddSongOverlay() {
        if !self.addSongOverlayActive {
            if let vc = self.addSongOverlayVC {
                vc.view.isHidden = false
                self.addSongOverlayActive = true
            }
        }
    }
    
    func hideAddSongOverlay() {
        if self.addSongOverlayActive {
            if let vc = self.addSongOverlayVC {
                vc.view.isHidden = true
                self.addSongOverlayActive = false
            }
        }
    }
}
