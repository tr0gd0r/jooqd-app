//
//  AddSongTableViewCell.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 1/15/17.
//  Copyright © 2017 jooqd. All rights reserved.
//

import UIKit

class AddSongTableViewCell: UITableViewCell {

    @IBOutlet weak var SongName: UILabel!
    @IBOutlet weak var SongArtist: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
