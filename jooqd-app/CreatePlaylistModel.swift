//
//  CreatePlaylistModel.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 3/27/17.
//  Copyright © 2017 jooqd. All rights reserved.
//

import CoreLocation
import Foundation

enum Vendors: String {
    case spotify = "spotify"
    case apple = "apple"
}

struct NewPlaylist {
    var vendor: Vendors
    var vendorAccessToken: String?
    var vendorRegionCode: String
    var playlistName: String
    var playlistLatitude: String
    var playlistLongtitude: String
}

extension NewPlaylist {
    var json: Data? {
        let vendor: String = self.vendor.rawValue
        let data: [String: String?] = [
            "vendor": vendor,
            "vendor_access_token": self.vendorAccessToken,
            "vendor_region_code": self.vendorRegionCode,
            "playlist_name": self.playlistName,
            "latitude": self.playlistLatitude,
            "longtitude": self.playlistLongtitude,
        ]
        do {
            return try JSONSerialization.data(withJSONObject: data, options: [])
        } catch _ {
            return nil
        }
    }
}

class PlaylistCreateModel {
    var vendorLoggedInWith: Vendors? = nil // set when auth is complete
    var vendorAuthorized: Bool = false
    var userLocation: CLLocation? = nil
    var region: String? = nil
    
    func submit(playlistName: String) {
        if !vendorAuthorized {
            print("vendor not authorized")
            return
        }
        
        let name = playlistName
        
        let vendor = self.vendorLoggedInWith! // force unwrap b/c vendorAuthorized must be true
        var lat: String!
        var long: String!
        var spotifyToken: String? = nil
        
        guard let region: String = self.region else {
            print("region missing")
            return
        }
        
        if let userLocation = self.userLocation {
            let coords = userLocation.coordinate
            lat = "\(coords.latitude)"
            long = "\(coords.longitude)"
        } else {
            print("missing user lat + long coordinates")
            return
        }
        
        if vendor == Vendors.spotify {
            if let session = Settings.spotifyAuth.session {
                spotifyToken = session.accessToken
            }
        }
        
        let newPlaylist = NewPlaylist(vendor: vendor,
                                      vendorAccessToken: spotifyToken,
                                      vendorRegionCode: region,
                                      playlistName: name,
                                      playlistLatitude: lat,
                                      playlistLongtitude: long)
        self.submitNewPlaylist(playlist: newPlaylist)
    }

    func submitNewPlaylist(playlist: NewPlaylist) {
        if let postData = playlist.json {
            let urlPath = "\(Settings.apiBase)/playlists/new"
            let url = URL(string: urlPath)
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.httpBody = postData
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            let task = URLSession.shared.dataTask(with: request, completionHandler: {data, response, error -> Void in
                let httpResponse:HTTPURLResponse? = response as? HTTPURLResponse
                guard let data = data else {
                    print("Data is empty")
                    return
                }
                let res = try! JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                if httpResponse?.statusCode == 200 {
                    let _vendor: String = res["vendor"] as! String
                    let vendor: Vendors! = Vendors(rawValue: _vendor) //idk why I have to force unwrap
                    let playlistId: String = res["playlist_id"] as! String
                    let playlistName: String = res["playlist_name"] as! String
                    let regionCode: String = res["region_code"] as! String
                    
                    Settings.isAdmin = true
                    Settings.isAdminForPlaylistId = playlistId
                    
                    let nc = NotificationCenter.default
                    nc.post(name: NSNotification.Name(rawValue: "PlaylistSelected"), object: nil, userInfo: ["playlistId": playlistId, "playlistName": playlistName, "vendor": vendor, "regionCode": regionCode])
                }
            })
            task.resume()
        }
    }
}
