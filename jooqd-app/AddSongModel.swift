//
//  AddSongModel.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 4/7/17.
//  Copyright © 2017 jooqd. All rights reserved.
//

import Foundation

struct SongResult {
    let id: String
    let name: String
    let album: String
    let albumArtUrl: String
    let artist: String
}

extension SongResult {
    var json: Data? {
        guard let vendor: Vendors = Settings.vendor else {
            return nil
        }
        let data: [String: String] = [
            "vendor": vendor.rawValue,
            "playlist_id": Settings.playlistId,
            "song_id": self.id,
            "song_name":  self.name,
            "artist": self.artist,
            "album": self.album,
            "album_art_url": self.albumArtUrl
        ]
        do {
            return try JSONSerialization.data(withJSONObject: data, options: [])
        } catch _ {
            return nil
        }
    }
}

protocol Searcher {
    var searchResults: [SongResult] { get set }
    
    func getSearchUrl(searchTerm: String) throws -> URL?
    func cleanSearchTerm(searchTerm: String) -> String
    func searchForSongs(searchUrl: URL)
}

protocol SongSearchDelegate: class {
    func updateResults()
    func songWasAccepeted()
    func songAlreadyInPlaylist()
}

enum SearchError: Error {
    case MissingVendorRegion
}

class BaseSearcher {
    weak var delegate: SongSearchDelegate?
    var searchResults: [SongResult] = []
    
    func submitNewSong(song: SongResult) {
        guard let postData = song.json else {
            return
        }
        let urlPath = "\(Settings.apiBase)/songs"
        let url = URL(string: urlPath)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = URLSession.shared.dataTask(with: request, completionHandler: {data, response, error -> Void in
            let httpResponse:HTTPURLResponse? = response as? HTTPURLResponse
            if let resCode = httpResponse?.statusCode {
                if let delegate = self.delegate {
                    switch resCode {
                    case 200:
                        delegate.songWasAccepeted()
                    case 409:
                        delegate.songAlreadyInPlaylist()
                    default:
                        print("POST to create new song failed with code: \(resCode)")
                        return
                    }
                }
            }
        })
        task.resume()
    }
}

class SpotifyMusicSearcher: BaseSearcher, Searcher {
    func getSearchUrl(searchTerm: String) throws -> URL? {
        let cleanedSearchTerm: String = self.cleanSearchTerm(searchTerm: searchTerm)
        let url: String = "\(Settings.apiBase)/sptquery/\(Settings.playlistId)/\(cleanedSearchTerm)"
        return URL(string: url)
    }
    
    func cleanSearchTerm(searchTerm: String) -> String {
        return searchTerm.replacingOccurrences(of: " ", with: "+")
    }
    
    func searchForSongs(searchUrl: URL) {
        let task = URLSession.shared.dataTask(with: searchUrl) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            do {
                var _searchResults: [SongResult] = []
                let response: Any = try JSONSerialization.jsonObject(with: data, options: [])
                guard let songs: [[String: Any]] = response as? [[String: Any]] else { return }
                for song in songs {
                    let id = song["id"] as! String
                    let name: String = song["name"] as! String
                    
                    let albumKey: [String: Any] = song["album"] as! [String: Any]
                    let album: String = albumKey["name"] as! String
                    
                    let artistsKey: [[String: Any]] = albumKey["artists"] as! [[String : Any]]
                    let artist: String = artistsKey[0]["name"] as! String
                    
                    let albumArtKey: [[String: Any]] = albumKey["images"] as! [[String : Any]]
                    let albumArtUrl: String = albumArtKey[0]["url"] as! String
                    
                    let song = SongResult(id: id, name: name, album: album, albumArtUrl: albumArtUrl, artist: artist)
                    _searchResults.append(song)
                }
                self.searchResults = _searchResults
                if let delegate = self.delegate {
                    delegate.updateResults()
                }
            } catch _ {
                return
            }
        }
        task.resume()
    }
}

class AppleMusicSearcher: BaseSearcher, Searcher {
    func getSearchUrl(searchTerm: String) throws -> URL? {
        guard let storefrontId = Settings.vendorRegion else {
            throw SearchError.MissingVendorRegion
        }
        let cleanedSearchTerm: String = self.cleanSearchTerm(searchTerm: searchTerm)
        let url: String = "https://itunes.apple.com/search?term=\(cleanedSearchTerm)&entity=song&s=\(storefrontId)"
        return URL(string: url)
    }
    
    func cleanSearchTerm(searchTerm: String) -> String {
        return searchTerm.replacingOccurrences(of: " ", with: "+")
    }
    
    func searchForSongs(searchUrl: URL) {
        let task = URLSession.shared.dataTask(with: searchUrl) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            do {
                var _searchResults: [SongResult] = []
                let response = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                let results: [[String: Any]] = response["results"] as! [[String : Any]]
                for result in results {
                    let isStreamable: Bool = result["isStreamable"] as! Bool
                    if isStreamable == false {
                        continue
                    }
                    let _id = result["trackId"] as! NSNumber
                    let id = "\(_id)"
                    let name: String = result["trackName"] as! String
                    let album: String = result["collectionName"] as! String
                    var albumArtUrl: String = result["artworkUrl100"] as! String
                    /* http://stackoverflow.com/questions/13382208/getting-bigger-artwork-images-from-itunes-web-search */
                    albumArtUrl = albumArtUrl.replacingOccurrences(of: "100x100bb", with: "500x500bb")
                    let artist: String = result["artistName"] as! String
                    let song = SongResult(id: id, name: name, album: album, albumArtUrl: albumArtUrl, artist: artist)
                    _searchResults.append(song)
                }
                self.searchResults = _searchResults
                if let delegate = self.delegate {
                    delegate.updateResults()
                }
            } catch _ {}
        }
        task.resume()
    }
}
