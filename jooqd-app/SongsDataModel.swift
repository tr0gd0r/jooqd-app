//
//  SongsDataModel.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 11/25/16.
//  Copyright © 2016 jooqd. All rights reserved.
//

import UIKit

struct Song {
    let name: String
    let artist: String
    var votes: Int
    let id: String
    let album: String
    let albumArtUrl: String
    var userDidVoteOnSong: Bool = false
    var userDidUpvote: Bool = false
    var userDidDownvote: Bool = false
    /*
     when someone votes or downvotes a song, we want to reflect it on their device immediately. so we
     change the votes count locally and then send out a http request for the server to incr or decr.
     that means something like the following could happen:
     - user upvotes a song
     - they see the count go up by one
     - playlist sync request returns
     - incr http request sent out
     - user sees count go down (back to what server has since it didn't get the incr request yet)
     - playlist sync request returns
     - user sees count go back up (since server now has their upvote)
     basically, we don't want a song a user has voted on to get synced until the server has their vote. that gives us two options:
     1) only sync playlist only after server gets incr/decr request
     2) allow sync to happen whenever but lock song so it's vote count doesn't get overwritten
    
     going with #2 for now
    */
    var locked: Bool = false

    init(name: String, artist: String, votes: Int, id: String, album: String, albumArtUrl: String) {
        self.name = name
        self.artist = artist
        self.votes = votes
        self.id = id
        self.album = album
        self.albumArtUrl = albumArtUrl
    }
}

class PlaylistModel {
    weak var delegate: SongsInPlaylistHandler?
    private let nc = NotificationCenter.default
    public var playlistId: String
    private var timer:Timer? = nil
    // songs indexed by their ID
    private var songs: [String: Song] = [:]
    // songs sorted by highest to lowest vote
    public var songsByVote: [Song] = []
    // song IDs that need up/down vote to be sent to server
    private var queueOfUpdates: [String] = []
    private var suspended: Bool = false
    
    init(playlistId: String) {
        self.playlistId = playlistId
        self.createTimer()
        nc.addObserver(self, selector: #selector(self.upvoteDownvoteSong), name: NSNotification.Name(rawValue: "SongWasVoted"), object: nil)
        nc.addObserver(self, selector: #selector(self.suspend), name: .UIApplicationWillResignActive, object: nil)
        nc.addObserver(self, selector: #selector(self.resume), name: .UIApplicationDidBecomeActive, object: nil)
    }
    
    func createTimer() {
        if self.timer == nil {
            self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: {t in
                self.syncPlaylist()
            })
        }
    }
    
    func syncPlaylist() {
        print("syncing playlist: \(self.playlistId)")
        let urlPath = "\(Settings.apiBase)/songs/\(Settings.playlistId)"
        let url = URL(string: urlPath)
        let session = URLSession.shared
        
        let task = session.dataTask(with: url!, completionHandler: {data, response, error -> Void in
            guard let data = data else {
                print("Data is empty")
                return
            }
            do {
                let res = try JSONSerialization.jsonObject(with: data, options: []) as! [Any]
                self.updateSongVotes(songsRes: res)
            } catch _ {return}
        })
        task.resume()
    }
    
    private func updateSongVotes(songsRes: [Any]) {
        /*
         1) old songs or currently playing will have to be deleted
         2) existing songs will have votes prop updated
         3) new songs will have to be added
         
         response from server is "source of truth" for what songs are in playlist. for each song in response from server, check to see
         if that song exists in current array client has. If song exists, update vote count. If song does not exist, it's new. Old songs
         (not in server response but in client's playlist) will be purged by not being added to new array.
         */
        
        var songs_: [String: Song] = [:]
        
        for s in songsRes {
            let s = s as! [String:Any]
            let votes = s["votes"] as! Int
            let artist = s["artist"] as! String
            let name = s["song_name"] as! String
            let id = s["song_id"] as! String
            let album = s["album"] as! String
            let albumArtUrl = s["album_art_url"] as! String
            
            if var existingSong = self.songs[id] {
                // song exists on client, update votes count with what server has
                // info is kept on if user up/down voted on it
                if !existingSong.locked {
                    // if it's locked, don't change vote prop
                    existingSong.votes = votes
                }
                songs_[id] = existingSong
            } else {
                // create new Song object
                songs_[id] = Song(name: name, artist: artist, votes: votes, id: id, album: album, albumArtUrl: albumArtUrl)
            }
        }
        
        self.songs = songs_
        // convert it to an array of Song objects sorted by votes for access by table view controller
        self.songsByVote = songs_.values.sorted(by: { s1, s2 in return s1.votes > s2.votes } )
        if let delegate = self.delegate {
            delegate.songsHaveChanged()
        }
    }
    
    func sendSongVote(song: Song) {
        if !song.userDidVoteOnSong {
            return
        }
        let songId: String = song.id
        var voteDirection: String = ""
        if song.userDidUpvote {
            voteDirection = "up"
        } else if song.userDidDownvote {
            voteDirection = "down"
        }
        let urlPath = "\(Settings.apiBase)/songs/\(Settings.playlistId)/\(songId)/\(voteDirection)"
        print("sending song vote: \(urlPath)")
        let url = URL(string: urlPath)
        var request = URLRequest(url: url!)
        request.httpMethod = "PATCH"
        let task = URLSession.shared.dataTask(with: request, completionHandler: {data, response, error -> Void in
            let httpResponse:HTTPURLResponse? = response as? HTTPURLResponse
            if httpResponse?.statusCode != 200 {
                // if update didn't work, put it back in the queue
                self.queueOfUpdates.append(songId)
            } else {
                if var s:Song = self.songs[songId] {
                    s.locked = false
                    self.songs[songId] = s
                }
            }
        })
        task.resume()
    }
    
    @objc func upvoteDownvoteSong(msg: Notification) {
        let songId: String = msg.userInfo?["songId"] as! String
        let direction: String = msg.userInfo?["direction"] as! String
        if var s:Song = self.songs[songId] {
            if direction == "up" {
                s.votes += 1
                s.userDidUpvote = true
            } else if direction == "down" {
                s.votes -= 1
                s.userDidDownvote = true
            }
            s.userDidVoteOnSong = true
            s.locked = true
            self.songs[songId] = s
            self.songsByVote = self.songs.values.sorted(by: { s1, s2 in return s1.votes > s2.votes } )
            if let delegate = self.delegate {
                delegate.songsHaveChanged()
            }
            self.queueOfUpdates.append(songId)
            self.checkQueue()
        }
    }
    
    func checkQueue() {
        for id in self.queueOfUpdates {
            if let s:Song = self.songs[id] {
                self.sendSongVote(song: s)
            }
        }
        self.queueOfUpdates.removeAll()
    }
    
    func shutDown() {
        if let t = timer {
            t.invalidate()
        }
    }
    
    @objc func suspend() {
        if self.suspended == false {
            self.suspended = true
            if let t = self.timer {
                t.invalidate()
            }
            self.timer = nil
        }
    }
    
    @objc func resume() {
        if self.suspended == true {
            self.suspended = false
            self.createTimer()
        }
    }
}

protocol SongsInPlaylistHandler: class {
    func songsHaveChanged()
}

