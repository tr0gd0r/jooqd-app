//
//  TabBar.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 12/19/16.
//  Copyright © 2016 jooqd. All rights reserved.
//

import UIKit

class TabBarManager: UITabBarController {
    let nc = NotificationCenter.default

    func decideWhichTabsToShow() {
        DispatchQueue.main.async {
            var newTabBar:[UIViewController] = []
            let playlistSelected:Bool = Settings.playlistId != ""
            for i in 0 ..< self.tabBar.items!.count {
                let tab:UITabBarItem = self.tabBar.items![i]
                let vc:UIViewController = self.viewControllers![i]
                
                if playlistSelected {
                    if tab.title == "Vote" && tab.isEnabled == false {
                        tab.isEnabled = true
                    }
                }
                
                if tab.title != "Play" {
                    newTabBar.append(vc)
                }
            }
            
            if Settings.isAdmin {
                let vc:UIViewController? = self.storyboard?.instantiateViewController(withIdentifier: "PlayerViewController")
                if let vc = vc {
                    newTabBar.append(vc)
                }
                /*
                 When you add view+tab programmatically, you have to set image + title
                 even though it was set in storyboard UI
                 */
                vc?.tabBarItem.image = #imageLiteral(resourceName: "play_icon")
                vc?.tabBarItem.title = "Play"
            }
            
            self.setViewControllers(newTabBar, animated: false)
            
            if playlistSelected {
                if Settings.isAdmin {
                    self.selectedIndex = 2
                } else {
                    self.selectedIndex = 1
                }
            }
        }
    }
    
    func updateActiveTabs(msg: Notification) {
        let id:String = msg.userInfo?["playlistId"] as! String
        let name:String = msg.userInfo?["playlistName"] as! String
        let vendor: Vendors = msg.userInfo?["vendor"] as! Vendors
        let regionCode: String = msg.userInfo?["regionCode"] as! String
        Settings.playlistId = id
        Settings.playlistName = name
        Settings.vendor = vendor
        Settings.vendorRegion = regionCode
        decideWhichTabsToShow()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nc.addObserver(self, selector: #selector(self.updateActiveTabs), name: NSNotification.Name(rawValue: "PlaylistSelected"), object: nil)
        decideWhichTabsToShow()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
