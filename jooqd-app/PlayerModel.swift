//
//  PlayerModel.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 4/7/17.
//  Copyright © 2017 jooqd. All rights reserved.
//

import Foundation
import MediaPlayer

protocol JooqdPlayer {
    associatedtype VendorPlayer
    var player: VendorPlayer { get }
    
    func playNewSong()
    func skipSong()
    func togglePlayState()
}

protocol JooqdPlayerDelegate: class {
    func newSongReceived(song: String, artist: String, albumArtUrl: String)
    func songFinishedPlaying()
}

struct TopSongToPlay {
    let id: String
    let name: String
    let album: String
    let artist: String
    let albumArtUrl: String
}

class BasePlayer {
    var songIsSet: Bool = false
    var songIsPlaying: Bool = false
    
    func getTopSong(completion: @escaping (TopSongToPlay) -> Void) {
        let urlPath = "\(Settings.apiBase)/songs/\(Settings.playlistId)/top"
        let url = URL(string: urlPath)
        let request = URLRequest(url: url!)
        let task = URLSession.shared.dataTask(with: request, completionHandler: {data, response, error -> Void in
            let httpResponse:HTTPURLResponse? = response as? HTTPURLResponse
            if let resCode = httpResponse?.statusCode {
                switch resCode {
                case 200:
                    let res = try! JSONSerialization.jsonObject(with: data!, options: []) as! [String:Any]
                    let songId: String = res["song_id"] as! String
                    let songName: String = res["song_name"] as! String
                    let album: String = res["album"] as! String
                    let artist: String = res["artist"] as! String
                    let albumArtUrl: String = res["album_art_url"] as! String
                    let song = TopSongToPlay(id: songId, name: songName, album: album, artist: artist, albumArtUrl: albumArtUrl)
                    completion(song)
                    
                    /*
                    if let delegate = self.delegate {
                        delegate.playThisSong(song: res)
                    }
                    */
                case 202:
                    /*
                     DispatchQueue.main.async {
                         let alertController = UIAlertController(title: "No song to play", message: "", preferredStyle: UIAlertControllerStyle.alert)
                         alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                         self.present(alertController, animated: true, completion: nil)
                     }
                     */
                    return
                default:
                    return
                }
            }
        })
        task.resume()
    }
}

class AppleMusicPlayer: BasePlayer, JooqdPlayer {
    // docs: You must use a music player only on your app’s main thread.
    typealias VendorPlayer = MPMusicPlayerController
    let player: VendorPlayer = MPMusicPlayerController.applicationMusicPlayer()
    weak var delegate: JooqdPlayerDelegate? = nil
    var progressTimer: Timer? = nil
    
    override init() {
        self.player.beginGeneratingPlaybackNotifications()
        self.player.repeatMode = .none // so when it stops it fetches next top song, doh!
    }
    
    func skipSong() {
        if self.songIsPlaying {
            self.player.stop()
            self.playNewSong()
        }
    }
    
    func playNewSong() {
        if let timer = self.progressTimer {
            timer.invalidate()
        }
        self.progressTimer = nil
        super.getTopSong(completion: self.topSongReceived)
    }
    
    func topSongReceived(song: TopSongToPlay) {
        let songToPlay: [String] = [song.id] // needs an array but we'll only every play/queue one song at a time
        self.player.setQueueWithStoreIDs(songToPlay)
        self.player.play()
        if let delegate = self.delegate {
            delegate.newSongReceived(song: song.name, artist: song.artist, albumArtUrl: song.albumArtUrl)
        }
        self.songIsSet = true
    }
    
    func pauseCurrentSong() {
        if self.songIsPlaying {
            self.player.pause()
        }
    }
    
    func resumeCurrentSong() {
        if !self.songIsPlaying {
            self.player.play()
        }
    }
    
    /*
     * if nothing is playing, get top song + play it
     * if song is playing, pause it
     * if song is paused, play it
     */
    func togglePlayState() {
        if !self.songIsSet {
            self.playNewSong()
        } else {
            if self.songIsPlaying {
                self.pauseCurrentSong()
            } else if !self.songIsPlaying {
                self.resumeCurrentSong()
            }
        }
    }
}

class SpotifyPlayer: BasePlayer, JooqdPlayer {
    typealias VendorPlayer = SPTAudioStreamingController
    let player: VendorPlayer = SPTAudioStreamingController.sharedInstance()
    weak var delegate: JooqdPlayerDelegate? = nil
    
    override init() {
        do {
            try self.player.start(withClientId: Settings.clientId)
        } catch {
            print("spotify player start failed")
            return
        }
        
        if let auth: SPTSession = Settings.spotifyAuth.session {
            self.player.login(withAccessToken: auth.accessToken)
        }
    }
    
    func skipSong() {
        if self.songIsPlaying {
            self.pauseCurrentSong()
            self.playNewSong()
        }
    }
    
    /*
     * if nothing is playing, get top song + play it
     * if song is playing, pause it
     * if song is paused, play it
     */
    func togglePlayState() {
        if !self.songIsSet {
            self.playNewSong()
        } else {
            if self.songIsPlaying {
                self.pauseCurrentSong()
            } else if !self.songIsPlaying {
                self.resumeCurrentSong()
            }
        }
    }
    
    func playNewSong() {
        super.getTopSong(completion: self.topSongReceived)
    }
    
    func topSongReceived(song: TopSongToPlay) {
        let spotifyUri:String = "spotify:track:\(song.id)"
        self.player.playSpotifyURI(spotifyUri, startingWith: 0, startingWithPosition: 0, callback: {error in
            if let error = error {
                self.songIsSet = false
                print("spotify player: playing song messed up: \(error)")
            }
        })
        if let delegate = self.delegate {
            delegate.newSongReceived(song: song.name, artist: song.artist, albumArtUrl: song.albumArtUrl)
        }
        self.songIsSet = true
    }
    
    func pauseCurrentSong() {
        if self.songIsPlaying {
            self.player.setIsPlaying(false, callback: { (error) in
                print("spotify player: error pausing song")
            })
        }
    }
    
    func resumeCurrentSong() {
        if !self.songIsPlaying {
            self.player.setIsPlaying(true, callback: { (error) in
                print("spotify player: error resuming song")
            })
        }
    }
    
}


