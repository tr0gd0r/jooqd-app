//
//  FirstViewController.swift
//  jooqd-app
//
//  Created by Anton Dorfman on 11/24/16.
//  Copyright © 2016 jooqd. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var joinBtn: UIButton!
    @IBOutlet weak var createBtn: UIButton!
    
    // MARK: Actions
    @IBAction func goToJoinPlaylist(_ sender: UIButton) {}
    
    @IBAction func returnToHome(segue: UIStoryboardSegue) {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Settings.backgroundColor
        
        joinBtn.backgroundColor = Settings.buttonBackgroundColor
        joinBtn.setTitleColor(Settings.fontColor, for: .normal)
        joinBtn.layer.cornerRadius = 5
        
        createBtn.backgroundColor = Settings.buttonBackgroundColor
        createBtn.setTitleColor(Settings.fontColor, for: .normal)
        createBtn.layer.cornerRadius = 5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

